public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager(){
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
		
	}	
	public String toString(){
			return "Center card: " + this.centerCard + "\n" +
			"Player card:" + this.playerCard;
	}	
	public void dealCards(){
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	public int getNumberOfCards(){
		return this.drawPile.length();
	}	
	public int calculatePoints(){
		if(this.centerCard.getValue().equals(this.playerCard.getValue())){
			return 4;
		}
		else if(this.centerCard.getSuit().equals(this.playerCard.getSuit())){
			return 2;
		}
		else return -1;
	}	
}	